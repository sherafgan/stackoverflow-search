import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import AppNavbar from "./AppNavbar";
import {Button, Container, Form, FormGroup, Input, Table} from 'reactstrap';
import checkSvg from './check.svg';
import uncheckSvg from './uncheck.svg';

class SearchQuery extends Component {
    emptyItem = {
        searchText: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem,
            results: [],
            isLoading: true
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = {...this.state.item};
        item[name] = value;
        this.setState({item});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const param = encodeURIComponent(this.state.item.searchText);
        fetch('api/search?searchText=' + param)
            .then(response => response.json())
            .then(data => this.setState({results: data, isLoading: false}));
    }

    render() {
        const {item} = this.state;

        let groupList = this.state.results.map(result => {
            return <tr key={result.id}>
                <td>
                    <img src={result.answered ? checkSvg : uncheckSvg}/>
                </td>
                <td>
                    <a href={result.linkToOrig} target="_blank">
                        <p>{result.title}</p>
                    </a>
                </td>
                <td>
                    <a href={result.authorLink} target="_blank">
                        <p>{result.authorName}</p>
                    </a>
                </td>
                <td>{result.date}</td>
            </tr>
        });

        return <div>
            <AppNavbar/>
            <Container>
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup className='searchRow'>
                        <Input type="text" name="searchText" id="searchText" value={item.searchText || ''}
                               onChange={this.handleChange} autoComplete="searchText"/>
                        <Button className='searchButton' color="primary" type="submit">Search</Button>{' '}
                    </FormGroup>
                </Form>
            </Container>
            <Container>
                <Container fluid>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="5%">Answer</th>
                            <th width="70%">Title</th>
                            <th>Author</th>
                            <th width="10%">Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        {groupList}
                        </tbody>
                    </Table>
                </Container>
            </Container>
        </div>
    }
}

export default withRouter(SearchQuery);