import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import SearchQuery from './SearchQuery';

class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact={true} component={SearchQuery}/>
                </Switch>
            </Router>
        )
    }
}

export default App;