### Stackoverflow /search API demo

http://api.stackexchange.com/docs/search#order=desc&sort=activity&intitle=java&filter=default&site=stackoverflow&run=true

##### Requirements
* [Java >8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Node.js >8 (LTS)](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/lang/en/docs/install/)

##### Run backend (localhost:8080)
```bash
./mvnw spring-boot:run
```


##### Run frontend app (localhost:3000)
```bash
cd app && \
yarn start
```

![SVSE](images/searchApiScreen.png)