package com.stackoverflow.search.web;

import com.stackoverflow.search.controller.QueryController;
import com.stackoverflow.search.model.QueryResult;
import com.stackoverflow.search.model.StackOverflowSearchResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RestApiBinder {
    private static Logger logger = LoggerFactory.getLogger(RestApiBinder.class);

    private static String stackExchangeApiTemplate =
            "http://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle=%s&site=stackoverflow";

    @GetMapping("/search")
    public List<QueryResult> search(@RequestParam String searchText) {
        logger.info("Accepted request param: " + searchText);
        String stackExchangeApiLink = String.format(stackExchangeApiTemplate, searchText.trim());
        logger.info("Requesting... " + stackExchangeApiLink);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        StackOverflowSearchResponse response = restTemplate.getForObject(stackExchangeApiLink, StackOverflowSearchResponse.class);
        logger.info("Finished requesting... " + (response != null));
        QueryController queryController = new QueryController(response);
        List<QueryResult> queryResults = queryController.getQueryResults();
        logger.info("Accepted " + queryResults.size() + " query results");
        return queryResults;
    }
}
