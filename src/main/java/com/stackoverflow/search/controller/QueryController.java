package com.stackoverflow.search.controller;

import com.stackoverflow.search.model.QueryResult;
import com.stackoverflow.search.model.StackOverflowSearchItem;
import com.stackoverflow.search.model.StackOverflowSearchItemOwner;
import com.stackoverflow.search.model.StackOverflowSearchResponse;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class QueryController {
    private StackOverflowSearchResponse stackOverflowSearchResponse;

    public QueryController(StackOverflowSearchResponse stackOverflowSearchResponse) {
        this.stackOverflowSearchResponse = stackOverflowSearchResponse;
    }

    public List<QueryResult> getQueryResults() {
        List<QueryResult> queryResults = new LinkedList<>();
        for (StackOverflowSearchItem item : stackOverflowSearchResponse.getItems()) {
            StackOverflowSearchItemOwner owner = item.getOwner();
            queryResults.add(new QueryResult(item.getQuestion_id(), item.getIs_answered(), item.getTitle(),
                    owner.getDisplay_name(), owner.getLink(), new Date(item.getCreation_date()).toString(), item.getLink()));
        }
        return queryResults;
    }
}
