package com.stackoverflow.search.model;

public class QueryResult {
    private Long id;
    private boolean isAnswered;
    private String title;
    private String authorName;
    private String authorLink;
    private String date;
    private String linkToOrig;

    public QueryResult(Long id, boolean isAnswered, String title, String author, String authorLink, String date, String linkToOrig) {
        this.id = id;
        this.isAnswered = isAnswered;
        this.title = title;
        this.authorName = author;
        this.authorLink = authorLink;
        this.date = date;
        this.linkToOrig = linkToOrig;
    }

    public Long getId() {
        return id;
    }

    public boolean isAnswered() {
        return isAnswered;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorLink() {
        return authorLink;
    }

    public String getDate() {
        return date;
    }

    public String getLinkToOrig() {
        return linkToOrig;
    }
}
